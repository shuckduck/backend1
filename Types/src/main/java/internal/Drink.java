package internal;

/**
 * Internal implementation of the Drink domain object.
 * @author Travis on 1/14/2018.
 */
public class Drink
{
	private Integer id;
	private String name;
	private String category;

	/**
	 * @return A builder to construct an immutable internal instance of {@link Drink}.
	 */
	public static DrinkBuilder builder()
	{
		return builder(null);
	}

	/**
	 * @param baseDrink a base drink object to fill in builder fields with
	 * @return A builder to construct an immutable internal instance of {@link Drink}.
	 */
	public static DrinkBuilder builder(Drink baseDrink)
	{
		return new DrinkBuilder(baseDrink);
	}

	/**
	 * Private constructor for the builder to use.
	 * @param builder object with set fields to create the drink object with
	 */
	private Drink(DrinkBuilder builder)
	{
		this.id = builder.id;
		this.name = builder.name;
		this.category = builder.category;
	}

	/**
	 * Builder class for {@link Drink}
	 */
	public static final class DrinkBuilder
	{
		private Integer id;
		private String name;
		private String category;

		/**
		 * Constructs a drink builder where fields are populated from a given drink object.
		 * @param drink object to fill in the new builder with
		 */
		private DrinkBuilder(Drink drink)
		{
			if (drink != null)
			{
				this.id = drink.id;
				this.name = drink.name;
				this.category = drink.category;
			}
		}

		/**
		 * @param id the id to set
		 * @return This builder with the id field set to the given value.
		 */
		public DrinkBuilder withId(Integer id)
		{
			this.id = id;
			return this;
		}

		/**
		 * @param name the name to set
		 * @return This builder with the name field set to the given value.
		 */
		public DrinkBuilder withName(String name)
		{
			this.name = name;
			return this;
		}

		/**
		 * @param category the category to set
		 * @return This builder with the category field set to the given value.
		 */
		public DrinkBuilder withCategory(String category)
		{
			this.category = category;
			return this;
		}

		/**
		 * @return An immutable instance of {@link Drink} with fields set by this builder.
		 */
		public Drink build()
		{
			return new Drink(this);
		}
	}
}
