package dao;

import connection.util.DBUtil;
import internal.Drink;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collection;

import static org.junit.Assert.*;

/**
 * @author Travis on 1/14/2018.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/persistenceContext-test.xml")
public class DrinkDaoTest
{
	@Autowired
	private DrinkDao drinkDao;

	@Test
	public void findAll() throws Exception
	{
		Collection<Drink> all = drinkDao.findAll();
		assertNotNull(all);
	}
}