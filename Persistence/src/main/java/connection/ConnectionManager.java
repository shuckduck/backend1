package connection;

import connection.util.DBUtil;

import java.sql.SQLException;

/**
 * @author Travis on 1/14/2018.
 */
public class ConnectionManager
{
	public ConnectionWrapper getConnection() throws SQLException
	{
		return new ConnectionWrapper(DBUtil.getDataSource().getConnection());
	}
}
