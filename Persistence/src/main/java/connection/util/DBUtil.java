package connection.util;

import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

/**
 * @author Travis on 1/14/2018.
 */
public class DBUtil
{
	private static final HikariDataSource dataSource;

	static
	{
		dataSource = new HikariDataSource();

		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/test");
		dataSource.setUsername("dbuser");
		dataSource.setPassword("password");
	}

	public static DataSource getDataSource()
	{
		return dataSource;
	}
}
