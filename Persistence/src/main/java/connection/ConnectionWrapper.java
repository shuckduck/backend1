package connection;

import query.Parameters;
import query.Query;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Travis on 1/14/2018.
 */
public class ConnectionWrapper
{
	private final Connection connection;

	public ConnectionWrapper(Connection connection)
	{
		this.connection = connection;
	}

	public ResultSet execute(Query query) throws SQLException
	{
		return execute(query, null);
	}

	public ResultSet execute(Query query, Parameters parameters) throws SQLException
	{
		return connection.createStatement().executeQuery(query.getSql(parameters));
	}
}
