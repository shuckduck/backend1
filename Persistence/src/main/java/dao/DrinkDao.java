package dao;

import connection.ConnectionManager;
import connection.ConnectionWrapper;
import internal.Drink;
import org.springframework.beans.factory.annotation.Autowired;
import query.Parameters;
import query.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

/**
 * @author Travis on 1/14/2018.
 */
public class DrinkDao extends Dao<Drink>
{
	private static final Query FIND_BY_ID = new Query("SELECT d.id, dn.name FROM drink d, drink_name dn WHERE d.id=:id AND d.nameId=dn.id", "id");
	private static final Query FIND_ALL = new Query("SELECT d.id, dn.name FROM drink d, drink_name dn WHERE d.nameId=dn.id");

	@Autowired
	private ConnectionManager connectionManager;

	@Override
	public Drink findById(int id) throws SQLException
	{
		ConnectionWrapper connection = connectionManager.getConnection();
		ResultSet resultSet = connection.execute(FIND_BY_ID, Parameters.builder(1).with("id", String.valueOf(id)).build());

		return toType(resultSet);
	}

	@Override
	public Collection<Drink> findAll() throws SQLException
	{
		return toTypeList(connectionManager.getConnection().execute(FIND_ALL));
	}

	@Override
	Drink toType(ResultSet resultSet) throws SQLException
	{
		return Drink.builder()
				.withId(resultSet.getInt("id"))
				.withName(resultSet.getString("name"))
				.build();
	}
}
