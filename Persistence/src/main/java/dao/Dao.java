package dao;

import connection.ConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Stuff Daos should have
 * @author Travis on 1/14/2018.
 */
abstract class Dao<T>
{
	public abstract T findById(int id) throws SQLException;

	public abstract Collection<T> findAll() throws SQLException;

	abstract T toType(ResultSet resultSet) throws SQLException;

	/**
	 * Returns a list of results from a given result set.
	 * @param resultSet the result set to parse into workable domain objects
	 * @return List of {@link T} objects created from the result set.
	 * @throws SQLException if an SQL exception occurs
	 */
	List<T> toTypeList(ResultSet resultSet) throws SQLException
	{
		List<T> results = Collections.emptyList();
		resultSet.last();
		int total = resultSet.getRow();
		if (total > 0)
		{
			results = new ArrayList<>(total);

			resultSet.beforeFirst();
			while (resultSet.next())
			{
				results.add(toType(resultSet));
			}
		}

		return results;
	}
}
