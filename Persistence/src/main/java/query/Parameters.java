package query;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Travis on 1/14/2018.
 */
public class Parameters
{
	private final Map<String, String> parameterMap;

	public static ParametersBuilder builder(int count)
	{
		return new ParametersBuilder(count);
	}

	private Parameters(ParametersBuilder builder)
	{
		parameterMap = builder.parameterMap;
	}

	public String getParameterValue(String parameterName)
	{
		return parameterMap.get(parameterName);
	}

	public int count()
	{
		return parameterMap.size();
	}

	public static final class ParametersBuilder
	{
		private final Map<String, String> parameterMap;

		private ParametersBuilder(int count)
		{
			parameterMap = new HashMap<>(count);
		}

		public ParametersBuilder with(String parameterName, String parameterValue)
		{
			parameterMap.put(parameterName, parameterValue);
			return this;
		}

		public Parameters build()
		{
			return new Parameters(this);
		}
	}
}
