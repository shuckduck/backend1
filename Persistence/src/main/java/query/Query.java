package query;

/**
 * @author Travis on 1/14/2018.
 */
public class Query
{
	private String sql;
	private final String[] parameterNames;

	public Query(String query, String... parameterNames)
	{
		this.sql = query;
		this.parameterNames = parameterNames;
	}

	public String getSql(Parameters parameters)
	{
		String resultingSql = String.valueOf(sql);
		if (parameters != null && parameters.count() > 0)
		{
			if (parameters.count() != parameterNames.length)
			{
				throw new IllegalArgumentException("Invalid number of parameter values.");
			}

			for (String parameterName : parameterNames)
			{
				resultingSql = sql.replaceAll(":" + parameterName, parameters.getParameterValue(parameterName));
			}
		}

		return resultingSql;
	}
}
